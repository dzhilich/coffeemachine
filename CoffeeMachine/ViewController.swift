//
//  ViewController.swift
//  CoffeeMachine
//
//  Created by Dmytro Zhylich on 24.04.2021.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let machine = CoffeeMachine()
        machine.menu()

        machine.makeADrink(DrinkType.americano)
        machine.makeADrink(DrinkType.espresso)
        machine.makeADrink(DrinkType.cappuccino)
        machine.makeADrink(DrinkType.hotMilk)

        machine.addMilk()
        machine.addCoffee()
        machine.addWater()

        machine.makeADrink(DrinkType.americano)
        machine.makeADrink(DrinkType.hotMilk)
        machine.makeADrink(DrinkType.hotMilk)

        machine.makeADrink(DrinkType.cappuccino)

        machine.addMilk()
        machine.addCoffee(xProportion: 5)

        machine.makeADrink(DrinkType.cappuccino)
        machine.makeADrink(DrinkType.espresso)
        machine.makeADrink(DrinkType.espresso)
        machine.makeADrink(DrinkType.espresso)
        machine.makeADrink(DrinkType.espresso)
        machine.makeADrink(DrinkType.espresso)

        machine.clearTrash()

        machine.makeADrink(DrinkType.espresso)
    }
}
