//
//  CoffeeMachine.swift
//  CoffeeMachine
//
//  Created by Dmytro Zhylich on 24.04.2021.
//

import Foundation


enum DrinkType: String {
    case cappuccino, espresso, americano, hotMilk

    var coffeeNeeded: Int {
        switch self {
        case .cappuccino: return 200
        case .americano: return 500
        case .espresso: return 1000
        case .hotMilk: return 0
        }
    }

    var waterNeeded: Int {
        switch self {
        case .cappuccino: return 50
        case .americano: return 500
        case .espresso: return 100
        case .hotMilk: return 0
        }
    }
    
    var milkNeeded: Int {
        switch self {
        case .cappuccino: return 500
        case .americano: return 0
        case .espresso: return 0
        case .hotMilk: return 1000
        }
    }
    
    var trashNeeded: Int {
        switch self {
        case .cappuccino: return 300
        case .americano: return 500
        case .espresso: return 1000
        case .hotMilk: return 0
        }
    }
}


class CoffeeMachine {
    var coffeeTank = 0
    var waterTank = 0
    var milkTank = 0
    var trashTank = 0
    
    let trashMaxTank = 4000
    
    func menu() {
        let line_length = 50
        let menu = "MENU"
        
        print("\n+\(String(repeating: "-", count: 48))+")
        let spacesLR = (line_length - menu.count - 2) / 2
        print("|\(String(repeating: " ", count: spacesLR))\(menu)\(String(repeating: " ", count: spacesLR))|")
        print("+\(String(repeating: "-", count: 48))+")
        
        let all_drinks = [
            DrinkType.americano,
            DrinkType.cappuccino,
            DrinkType.espresso,
            DrinkType.hotMilk
        ]
        
        for drink in all_drinks {
            var alignment = 1
            if drink.rawValue.count % 2 == 0 {
                alignment = 0
            }
            let spacesLR = (line_length - drink.rawValue.count - 2) / 2
            print("|\(String(repeating: ".", count: spacesLR))\(drink)\(String(repeating: ".", count: spacesLR + alignment))|")
        }
        print("+\(String(repeating: "-", count: 48))+\n")
    }
    
    private func showTankInfo(){
        print("+---------------------------+")
        print("| Tanks statuses")
        print("+---------------------------+")
        print("| left coffee:", coffeeTank)
        print("| left water:", waterTank)
        print("| left milk:", milkTank)
        print("|")
        print("| trash filled on:", trashTank)
        print("+---------------------------+")
    }

    func makeADrink(_ drink: DrinkType) {
        print(String(repeating: "*", count: 25))
        print("* Make -", drink)
        print(String(repeating: "*", count: 25))
        
        var is_success = true
        
        if coffeeTank < drink.coffeeNeeded {
            print("# - Please add coffee")
            is_success = false
        }
        if waterTank < drink.waterNeeded {
            print("# - Please add water")
            is_success = false
        }
        if milkTank < drink.milkNeeded {
            print("# - Please add milk")
            is_success = false
        }
        if trashTank >= trashMaxTank {
            print("# - Please clear trash tank")
            is_success = false
        }
        
        if is_success {
            print("*** the drink `\(drink)` is ready ***")
            trashTank = trashTank + drink.trashNeeded
            coffeeTank = coffeeTank - drink.coffeeNeeded
            waterTank = waterTank - drink.waterNeeded
            milkTank = milkTank - drink.milkNeeded
        }
        
        print(String(repeating: "*", count: 50))
    }
    
    func addMilk(xProportion: Int = 1){
        milkTank += 2000 * xProportion
        
        print("\n+++ MILK is added\n")
        showTankInfo()
    }
    func addCoffee(xProportion: Int = 1){
        coffeeTank += 1000 * xProportion
        
        print("\n+++ COFFEE is added\n")
        showTankInfo()
    }
    func addWater(xProportion: Int = 1){
        waterTank += 4000 * xProportion
        
        print("\n+++ WATER is added\n")
        showTankInfo()
    }
    func clearTrash(){
        trashTank = 0
        
        print("\n+++ TRASH tank is cleared\n")
        showTankInfo()
    }
}
